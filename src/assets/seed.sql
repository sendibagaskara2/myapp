CREATE TABLE IF NOT EXISTS developer(id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT,skills TEXT,img TEXT);
INSERT or IGNORE INTO developer VALUES (1, 'Simon', '', 'https://fontawesome.com/icons/user-tie?style=solid');
INSERT or IGNORE INTO developer VALUES (2, 'Max', '', 'https://fontawesome.com/icons/user-tie?style=solid');
INSERT or IGNORE INTO developer VALUES (3, 'Ben', '', 'https://fontawesome.com/icons/user-tie?style=solid');
 
CREATE TABLE IF NOT EXISTS product(id INTEGER PRIMARY KEY AUTOINCREMENT,name TEXT, creatorId INTEGER);
INSERT or IGNORE INTO product(id, name, creatorId) VALUES (1, 'PHP', 1);
INSERT or IGNORE INTO product(id, name, creatorId) VALUES (2, 'Software Startup Manual', 1);
INSERT or IGNORE INTO product(id, name, creatorId) VALUES (3, 'Ionic Framework', 2);
INSERT or IGNORE INTO product(id, name, creatorId) VALUES (4, 'Codeignether', 2);
INSERT or IGNORE INTO product(id, name, creatorId) VALUES (5, 'Python', 3);
INSERT or IGNORE INTO product(id, name, creatorId) VALUES (6, 'JavaScript', 3);